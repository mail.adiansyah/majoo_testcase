<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use App\Models\Product;
use App\Models\User;

class BasicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'admin',
            'email'=>'admin@gmail.com',
            'password'=>bcrypt('password'),
        ]);

        $category = Category::create([
            'name'=>'category 1',
            'description'=>'Eveniet cum consequatur earum sequi similique molestiae consequuntur.',
        ]);

        $category2 = Category::create([
            'name'=>'category 2',
            'description'=>'Facilis suscipit similique ut quasi magni.',
        ]);

        Product::create([
            'name'=>'majoo Pro',
            'description'=>"<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled</p>",
            'price'=>2750000,
            'category_id'=>$category->id,
            'image'=>'majoo Pro.png',
        ]);

        Product::create([
            'name'=>'majoo Advance',
            'description'=>"<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum</p>",
            'price'=>5000000,
            'category_id'=>$category->id,
            'image'=>'majoo Advance.png',
        ]);

        Product::create([
            'name'=>'majoo Lifestyle',
            'description'=>"<p>and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>",
            'price'=>7000000,
            'category_id'=>$category->id,
            'image'=>'majoo Lifestyle.png',
        ]);

        Product::create([
            'name'=>'majoo Desktop',
            'description'=>"<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered</p>",
            'price'=>10000000,
            'category_id'=>$category->id,
            'image'=>'majoo Desktop.png',
        ]);
    }
}
