<?php

namespace Database\Factories;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $categories = Category::all()->pluck('id')->toArray();
        return [
            'name' => $this->faker->unique()->word(),
            'description' => $this->faker->sentence(),
            'price' => $this->faker->numberBetween(100000, 1000000),
            'category_id' => $this->faker->randomElement($categories),
            'image' => $this->faker->randomElement([
                'paket-desktop.png', 
                'standard_repo.png', 
                'paket-lifestyle.png', 
                'paket-advance.png',
            ]),
        ];
    }
}
