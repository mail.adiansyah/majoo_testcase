how to run :
1. clone repository
2. create file .env from .env.example
3. change database configuration in file .env, make sure the database is already created or create new one
4. run following commands
php artisan key:generate
composer update
composer dump-autload
php artisan migrate
php artisan db:seed --class=BasicSeeder
php artisan serve
6. access the url http://localhost:8000
7. default password admin
email : admin@gmail.com
password : password