<?php

namespace App\DataTables;

use App\Models\Product;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('category.name', function($query) {
                if ($query->category) {
                    return $query->category->name;
                }else{
                    return "";
                }
            })
            ->rawColumns(['image', 'action'])
            ->addColumn('action', 'products.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Product $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        return $model->newQuery()->with('category');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('products-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->parameters([
                        "scrollX"=> true,
                        'lengthMenu' => [[10, 50, 100, -1], ['10', '50', '100', "All"]],
                        'buttons' => [
                            ['extend' => 'create', 'className' => 'btn btn-primary me-2 mb-2'],
                            ['extend' => 'csv', 'className' => 'btn btn-info text-white mb-2'],
                        ],
                        'columnDefs' => [
                            [
                                "targets" => [4],
                                "render" => "$.fn.dataTable.render.number('.', ',', '', '')"
                            ],
                            [
                                'targets' => [4],
                                'className' => 'text-end',
                            ],
                            [
                                'targets' => [3],
                                'className' => 'text-description',
                            ],
                            [
                                "render"=> "function(data, type, row, meta){
                                    if(type === 'display'){
                                        data = `<img class='thumbnail' src='".url('/')."/images/products/`+data+`'>`;
                                    }
                                    return data;
                                }",
                                "targets"=> [6],
                            ],
                        ],
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->width(60)
                ->addClass('text-center'),
            Column::make('id'),
            Column::make('name'),
            Column::make('description'),
            Column::make('price'),
            Column::make('category.name')
                ->title('Category'),
            Column::make('image'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Products_' . date('YmdHis');
    }
}