<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\DataTables\ProductsDataTable;
use Illuminate\Http\Request;
use Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ProductsDataTable $dataTable)
    {
        return $dataTable->render('products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $product = new Product();
        // get list categories
        $categories = Category::pluck('name', 'id');
        
        return view('products.create', compact('product', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        try {
            \DB::beginTransaction();
            $input = $request->validated();
            
            $extension = $request->image->getClientOriginalExtension();
            $filename = $request->name.'.'.$extension;
            
            $input['image'] = $filename;
            $product = Product::create($input);

            // do upload image
            $request->image->storeAs('images/products', $filename, 'public_uploads');

            \DB::commit();

            alert()->success('Success', 'Product created successfully');
            return redirect()->route('products.index');

        } catch (\Throwable $e) {
            report($e);
            \DB::rollBack();
            
            alert()->error('Error', 'Failed to create product');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        // get list categories
        $categories = Category::pluck('name', 'id');

        return view('products.show', compact('categories', 'product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        // get list categories
        $categories = Category::pluck('name', 'id');

        return view('products.edit', compact('categories', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProductRequest  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        try {
            \DB::beginTransaction();
            $input = $request->validated();
            
            // check if new image uploaded
            if ($request->hasFile('image')) {
                $extension = $request->image->getClientOriginalExtension();
                $filename = $request->name.'.'.$extension;
                
                $input['image'] = $filename;
            }

            $product->update($input);

            // do upload image if new image exist
            if ($request->hasFile('image')) {
                $request->image->storeAs('images/products', $filename, 'public_uploads');
            }

            \DB::commit();

            alert()->success('Success', 'Product created successfully');
            return redirect()->route('products.index');

        } catch (\Throwable $e) {
            report($e);
            \DB::rollBack();
            
            alert()->error('Error', 'Failed to create product');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {
            $product->delete();

            alert()->success('Success', 'Product deleted successfully');
            return redirect()->route('products.index');

        } catch (\Throwable $e) {
            report($e);
            alert()->error('Error', 'Failed to delete Product');
            return redirect()->back()->withInput();
        }
    }

    /**
     * Upload image to server.
     *
     * @param  \App\Models\Product  $product
     * @return json
     */
    public function uploadImage(Request $request)
    {
        if (isset($_POST['btnSubmit'])) {
        $uploadfile = $_FILES["uploadImage"]["tmp_name"];
        $folderPath = "uploads/";
        
        if (! is_writable($folderPath) || ! is_dir($folderPath)) {
            echo "error";
            exit();
        }
        if (move_uploaded_file($_FILES["uploadImage"]["tmp_name"], $folderPath . $_FILES["uploadImage"]["name"])) {
            echo '<img src="' . $folderPath . "" . $_FILES["uploadImage"]["name"] . '">';
            exit();
        }
    }
    }
}
