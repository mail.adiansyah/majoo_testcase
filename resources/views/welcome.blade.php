@extends('layouts.app')

@section('css')
    {{-- load css from layouts parent here --}}
    @parent

    <style type="text/css">

        .img-product {
            width: 100%;
        }

        @media (min-width: 768px) {
            .card-product {
                height: 550px;
            }

            .text-description {
                display: -webkit-box;
                -webkit-line-clamp: 8;
                -webkit-box-orient: vertical;
                overflow: hidden;
                font-size: 0.85rem;
            }
        }

        @media (min-width: 991px) {
            .card-product {
                height: 500px;
            }
            
            .text-description {
                display: -webkit-box;
                -webkit-line-clamp: 8;
                -webkit-box-orient: vertical;
                overflow: hidden;
                font-size: 0.85rem;
            }
        }

        @media (min-width: 1200px) {
            .card-product {
                height: 500px;
            }
            
            .text-description {
                display: -webkit-box;
                -webkit-line-clamp: 8;
                -webkit-box-orient: vertical;
                overflow: hidden;
                font-size: 0.85rem;
            }
        }

        @media (min-width: 1400px) {
            .card-product {
                height: 550px;
            }
            
            .text-description {
                display: -webkit-box;
                -webkit-line-clamp: 8;
                -webkit-box-orient: vertical;
                overflow: hidden;
                font-size: 0.85rem;
            }
        }
    </style>
@endsection

@section('js')
    {{-- load js from layouts parent here --}}
    @parent

    {{-- put custom js per page here --}}
@endsection

@section('content')
<div class="container">
    <div class="row pb-3">
        <div class="col-md-12">
            <h3>Produk</h3>
        </div>
    </div>
    <div class="row">
        {{-- loop for products here --}}
        @foreach($products as $key=>$val)
        <div class="col-lg-3 col-md-6 pb-md-3 pb-sm-3">
            <div class="card card-product">
                <img src="{{url('/').'/images/products/'.$val->image}}" class="card-img-top img-product" alt="...">
                <div class="card-body d-flex flex-column">
                    <div class="row pb-2">
                        <div class="col-lg-12 text-center">
                            <h5 class="card-title">{{$val->name}}</h5>
                        </div>
                    </div>
                    <div class="row pb-2">
                        <div class="col-lg-12 text-center">
                            <h5>Rp {{number_format(floor($val->price),0,'.',',')}}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-description">
                            {!!$val->description!!}
                        </div>
                    </div> 
                    <div class="row mt-auto">
                        <div class="col-lg-12 text-center">
                            <a href="#" class="btn btn-primary">Beli</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
