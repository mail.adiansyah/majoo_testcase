@extends('layouts.app')

@section('css')
    {{-- load css from layouts parent here --}}
    @parent

    {{-- put custom css per page here --}}
@endsection

@section('js')
    {{-- load js from layouts parent here --}}
    @parent

    {{-- put custom js per page here --}}
    <script>
        $(document).ready(function() {
            // disable form input
            $('#form-category input').prop('disabled', true);
            $('#form-category select').prop('disabled', true);
            $('#form-category textarea').prop('disabled', true);
        });
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Detail Category') }}</div>

                <div class="card-body">
                    <form class="form" id="form-category" action="" method="POST">
                        @include('categories._form', ['show'=>true])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
