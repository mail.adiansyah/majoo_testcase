<a href="{{ route('categories.show', $id) }}" title="Show detail"><i class="bi bi-eye"></i></a>
<a href="{{ route('categories.edit', $id) }}" title="Edit"><i class="bi bi-pencil-square"></i></a>
<a href="#" data-href="{{ route('categories.destroy', $id) }}" title="Delete" class="btn-delete"><i class="bi bi-trash"></i></a>