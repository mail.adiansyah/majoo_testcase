@extends('layouts.app')

@section('css')
    {{-- load css from layouts parent here --}}
    @parent

    {{-- put custom css per page here --}}
    {{-- ckeditor for input description --}}
    <style type="text/css">
        .ck-editor__editable_inline {
            min-height: 150px;
        }
    </style>
@endsection

@section('js')
    {{-- load js from layouts parent here --}}
    @parent

    {{-- put custom js per page here --}}
    {{-- ckeditor for input description --}}
    <script src="https://cdn.ckeditor.com/ckeditor5/31.1.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#description' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Edit Category') }}</div>

                <div class="card-body">
                    <form class="form" id="form-category" action="{{ route('categories.update', $category) }}" method="POST">
                        @csrf
                        @method('PUT')
                        @include('categories._form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
