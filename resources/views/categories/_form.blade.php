<div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control" id="name" placeholder="" name="name" value="{{ old('name', $category->name) }}">
    <div class="invalid-feedback">
      Please enter a message in the textarea.
    </div>
    @if ($errors->has('name'))
    <small class="text-danger">{{ $errors->first('name') }}</small>
    @endif
</div>
<div class="mb-3">
    <label for="description" class="form-label">Description</label>
    <textarea class="form-control" id="description" rows="3" name="description">{{ old('description', $category->description) }}</textarea>
    @if ($errors->has('description'))
    <small class="text-danger">{{ $errors->first('description') }}</small>
    @endif
</div>
<div class="pt-3 float-end">
    <a href="{{ route('categories.index') }}"><button type="button" class="btn btn-light">Kembali</button></a>
    @isset($show)
    {{-- this button only on show --}}
    <a href="{{ route('categories.edit', $category) }}"><button type="button" class="btn btn-primary">Edit</button></a>
    @else
    <button type="submit" class="btn btn-primary">Submit</button>
    @endif
</div>

