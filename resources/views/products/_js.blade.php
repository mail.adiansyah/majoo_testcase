<script type="text/javascript">
    {{-- js choose image --}}
    $(document.body).on('change', '.upload-input', function (e) {
        var fuData = this;
        var FileUploadPath = $(this).val();

        if (FileUploadPath == '') {
            Swal.fire('Error','Choose image','warning');
        } else {
            var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

            if (this.files && this.files[0]) {
                var reader = new FileReader();            
                reader.onload = function (e) {
                    $('#upload-area').css('background-image','url('+ e.target.result +')');
                }            
                reader.readAsDataURL(this.files[0]);
            }
        }
    });
</script>