@extends('layouts.app')

@section('css')
    {{-- load css from layouts parent here --}}
    @parent

    {{-- put custom css per page here --}}
    {{-- ckeditor for input description --}}
    <style type="text/css">
        .ck-editor__editable_inline {
            min-height: 150px;
        }
    </style>

    @include('products._css')
@endsection

@section('js')
    {{-- load js from layouts parent here --}}
    @parent

    {{-- put custom js per page here --}}
    {{-- ckeditor for input description --}}
    <script src="https://cdn.ckeditor.com/ckeditor5/31.1.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#description' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>

    @include('products._js')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Edit Product') }}</div>

                <div class="card-body">
                    <form class="form" id="form-product" action="{{ route('products.update', $product) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        @include('products._form')
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
