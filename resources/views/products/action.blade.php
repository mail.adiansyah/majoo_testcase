<a href="{{ route('products.show', $id) }}" title="Show detail"><i class="bi bi-eye"></i></a>
<a href="{{ route('products.edit', $id) }}" title="Edit"><i class="bi bi-pencil-square"></i></a>
<a href="#" data-href="{{ route('products.destroy', $id) }}" title="Delete" class="btn-delete"><i class="bi bi-trash"></i></a>