@extends('layouts.app')

@section('css')
    {{-- load css from layouts parent here --}}
    @parent
    
    {{-- put custom css per page here --}}
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">

    <style type="text/css">
        /*image of products*/
        img.thumbnail {
            width: 100px;
        }

        /*description max line to show*/
        .text-description {
            min-width: 300px;
        }
    </style>
@endsection

@section('js')
    {{-- load js from layouts parent here --}}
    @parent

    {{-- put custom js per page here --}}
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <script src="{{ asset('vendor/datatables/buttons.server-side.js') }}"></script>

    {{-- render script datatable from yajra --}}
    {{$dataTable->scripts()}}

    {{-- delete action --}}
    <script type="text/javascript">
        $(document.body).on("click", ".btn-delete",function() {
            var url = $(this).data('href');

            Swal.fire({
                title: 'Delete product',
                text: 'Are you sure to delete this product ?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Delete',
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#form-group-delete').attr('action', url);
                    $('#form-group-delete').submit();
                }
            });
        });
    </script>

    {{-- remove db-button class --}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('.dt-button').removeClass('dt-button');
        });
    </script>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">{{ __('List Products') }}</div>

                <div class="card-body" style="font-size: 0.85rem;">
                    {{-- render datatable from yajra --}}
                    {{$dataTable->table(['class' => 'table table-bordered', 'width' => '100%'])}}
                </div>
            </div>
        </div>
    </div>
</div>

{{-- form delete, populated from jquery event click button delete --}}
<form id="form-group-delete" action="" method="post" class="d-none">
    @csrf
    @method('DELETE')
</form>
@endsection
