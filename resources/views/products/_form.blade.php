<div class="mb-3">
    <label for="name" class="form-label">Name</label>
    <input type="text" class="form-control" id="name" placeholder="" name="name" value="{{ old('name', $product->name) }}">
    <div class="invalid-feedback">
      Please enter a message in the textarea.
    </div>
    @if ($errors->has('name'))
    <small class="text-danger">{{ $errors->first('name') }}</small>
    @endif
</div>
<div class="mb-3">
    <label for="description" class="form-label">Description</label>
    <textarea class="form-control" id="description" rows="3" name="description">{{ old('description', $product->description) }}</textarea>
    @if ($errors->has('description'))
    <small class="text-danger">{{ $errors->first('description') }}</small>
    @endif
</div>
<div class="mb-3">
    <label for="price" class="form-label">Price</label>
    <div class="input-group">
        <span class="input-group-text">Rp</span>
        <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" name="price" value="{{ old('price', $product->price) }}">
        <span class="input-group-text">.00</span>
    </div>
</div>
<div class="mb-3">
    <label for="category_id" class="form-label">Category</label>
    <select class="form-select {{isset($show) ? '' : 'select2'}}" aria-label="Default select example" name="category_id">
        <option selected>Open this select menu</option>
        @foreach ($categories as $key=>$val)
        <option value="{{$key}}" {{old('category_id', $product->category_id) == $key ? 'selected' : ''}}>{{$val}}</option>
        @endforeach
    </select>
    @if ($errors->has('category_id'))
    <small class="text-danger">{{ $errors->first('category_id') }}</small>
    @endif
</div>
<div class="mb-3">
    <label for="image" class="form-label">Image</label>
     
    <div class="upload-area d-flex flex-column" id="upload-area" style="
    background-image: url('{{ $product->image ? url('images/products').'/'.$product->image : '' }}');
    ">
        <label for="upload-image" class="btn-upload-mms"><i class="bi bi-cloud-arrow-up"></i><br></label>
        <center></center>
    </div>

    <input type="file" class="upload-input d-none" id="upload-image" name="image">
    <small class="text-muted">File type: JPEG, JPG, GIF;<br>Suggested dimension: 296 x 180 pixels</b></small>

    {{-- <div class="input-group mb-3">
        <img src="..." class="rounded float-start" alt="...">
        <input type="file" class="form-control" id="image" name="image">
        <label class="input-group-text" for="image">Upload</label>
    </div> --}}
    @if ($errors->has('image'))
    <small class="text-danger">{{ $errors->first('image') }}</small>
    @endif
</div>

<div class="pt-3 float-end">
    <a href="{{ route('products.index') }}"><button type="button" class="btn btn-light">Kembali</button></a>
    @isset($show)
    {{-- this button only on show --}}
    <a href="{{ route('products.edit', $product) }}"><button type="button" class="btn btn-primary">Edit</button></a>
    @else
    <button type="submit" class="btn btn-primary">Submit</button>
    @endif
</div>

